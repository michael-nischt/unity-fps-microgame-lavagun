# Unity FPS Microgame Lavagun

A two day attempt extend the Unity FPS Microgame project with a new “lava gun” weapon featuring a particle stream, burn decals and tweaked the shooting behavior. In all, a fun and neat looking addition.

[![Image-1](README.res/lavagun_001_preview.png)](README.res/lavagun_001.png)
[![Image-2](README.res/lavagun_002_preview.png)](README.res/lavagun_002.png)
[![Image-3](README.res/lavagun_003_preview.png)](README.res/lavagun_003.png)

[![Image-4](README.res/lavagun_004_preview.png)](README.res/lavagun_004.png)
[![Image-5](README.res/lavagun_005_preview.png)](README.res/lavagun_005.png)
[![Image-6](README.res/lavagun_006_preview.png)](README.res/lavagun_006.png)

## Inspirations

[ProDeus - Lava gun!](https://youtu.be/aWEcxjeIKuk?t=1125)

I first looked for games with some kind of lava gun and found ProDeus, which shoots lava bubbles with an area of effect damage and decal. I almost went for that but the MicroFPS game’s launcher weapon is very similar and it would have been mostly a reskin from the ‘disc’ to a ‘bubble’ and adding a decal for the impact.

[Roblox Base Battles Lava Gun](https://youtu.be/0mbfkSXMF5g?t=185)

The next finding was a Lavagun in Roblox. However, it doesn’t look great and more like a laser beam. Which could be great to cut things in the environment but unless its the core of the game too costly for mobile.

[Mario Sunshine Watergun (F.L.U.D.D.)](https://youtu.be/wx3PtRjQplM?t=35)

I wanted something a bit more like a stream of lava. Similar to shooting the watergun in Mario Sunshine. 

[Lava Lamp Shader](https://youtu.be/E-5-omc0lgg)

But with the particles more connected (line strip) and more shape variations. Kinda like a very dense lava lamp. I looked at this shader and decided I can use some of its logic and change the input texture (channels) for a higher density.

## Choices / Decisions

I decided against external assets (e.g. asset store) since it was quicker to re-bash existing ones than to search for fitting ones. Especially within the art style. Assets touched / recombined Include models, vfx/sfx and the weapon / hud icons. As it was already done in the project. E.g. The shotgun is a modified blaster visually.

I put my focus on the Lava stream, particularly its shape and shader generated texture. I decided to enhance the impact with hit decals even though the other weapons don’t (yet) have them. 
The stream shape is generated via a series of individual lines which connect to each other. At first I was considering a single line-strip but I really wanted a lava stream with gaps. Be it shooting in bursts or if the stream briefly gets hit by something like a column in the world.
The shader material is a simple sliding texture which has a different pattern in each channel (RGBA). Combining two at different speeds and using the others for distortions to achieve the look of a fluid. The last step remaps intensity to 2 colors and alpha transparency used for a cut-off which is cheaper on mobile than full alpha blending. There are a few possible minor performance improvements possible for the shader graph by re-using node-inputs but reducing readability as connections will cross all over. As the shader is still rather lightweight / mobile friendly I decided against it but it is possible for a few gains. Some settings like the colors and sliding speeds could be exposed to the blackboard if it would be reused for another stream type (e.g. water).

Finally, I tweaked the weapon script settings in regards to ammo, shooting mechanics and linked matching vfx/sfx. I was considering adding a new dedicated weapon type but was perfectly happy with one of the existing.

Overall, I believe I succeeded in creating a unique weapon fitting the existing ones instead of just replacing the blaster.

## Problems / Challenges

I’m happy with the outcome after spending ~15h on the task. However, many aspects can be improved. Especially on the look & feel side. I would probably start with the lava shader and shape (line-strip) and the weapons settings.

I didn't have the time to properly profile the lavagun. Especially in comparison to the other three weapons. While the lavagun might be slightly more costly than the others, overall I doubt it has any impact on the total performance and should work well on mobile devices. If then enabling the decal renderer has the biggest impact but it should be a mobile target feature. Especially since the built-in RP’s projectors worked well if limited to a few.
What makes the profiling task harder is that I would need to add custom profile markers since the Unity FPS Microgame project itself is rather bad in regards to performance. Hence it goes beyond the scope of the goal.

## Notes

The lavagun was quite a fun task. I liked that it involved aspects ranging from game-design, had some small artistic parts and all glued together with code. I very enjoy that on a job as well but in the long run prefer a bit more algorithmic / systemic challenges.
