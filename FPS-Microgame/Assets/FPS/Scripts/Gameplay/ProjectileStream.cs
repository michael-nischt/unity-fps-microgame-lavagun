﻿using System.Collections.Generic;
using Unity.Collections;
using Unity.FPS.Game;
using UnityEngine;

namespace Unity.FPS.Gameplay
{
    public class ProjectileStream : ProjectileBase
    {
        [Tooltip("Line Renderer reference")]
        public LineRenderer Line;

        [Tooltip("LifeTime of the projectile")]
        public float MaxLifeTime = 5f;

        [Tooltip("VFX prefab to spawn upon impact")]
        public GameObject ImpactVfx;

        [Tooltip("LifeTime of the VFX before being destroyed")]
        public float ImpactVfxLifetime = 5f;

        [Tooltip("Offset along the hit normal where the VFX will be spawned")]
        public float ImpactVfxSpawnOffset = 0.1f;

        [Tooltip("Clip to play on impact")]
        public AudioClip ImpactSfxClip;

        [Tooltip("Layers this projectile can collide with")]
        public LayerMask HittableLayers = -1;

        [Header("Movement")] [Tooltip("Speed of the projectile")]
        public float Speed = 20f;

        [Tooltip("Downward acceleration from gravity")]
        public float GravityDownAcceleration = 9.81f;

        [Header("Damage")] [Tooltip("Damage of the projectile")]
        public float Damage = 40f;

        [Tooltip("Area of damage. Keep empty if you don<t want area damage")]
        public DamageArea AreaOfDamage;

        ProjectileBase m_ProjectileBase;
        float m_WeaponDelayBetweenShots;

        float m_ShootTime;
        List<Collider> m_IgnoredColliders;

        const QueryTriggerInteraction k_TriggerInteraction = QueryTriggerInteraction.Collide;

        public override void Shoot(WeaponController controller)
        {
            m_WeaponDelayBetweenShots = controller.DelayBetweenShots;
            base.Shoot(controller);
        }

        void OnEnable()
        {
            m_ProjectileBase = GetComponent<ProjectileBase>();
            DebugUtility.HandleErrorIfNullGetComponent<ProjectileBase, ProjectileStandard>(m_ProjectileBase, this,
                gameObject);

            m_ProjectileBase.OnShoot += OnShoot;

            Destroy(gameObject, MaxLifeTime);
        }

        new void OnShoot()
        {
            m_ShootTime = Time.time;

            m_IgnoredColliders = new List<Collider>();
            // Ignore colliders of owner
            Collider[] ownerColliders = m_ProjectileBase.Owner.GetComponentsInChildren<Collider>();
            m_IgnoredColliders.AddRange(ownerColliders);
        }


        void Update()
        {
            using(var points_r = new NativeArray<Vector3>(2, Allocator.Temp))
            {
                NativeArray<Vector3> points = points_r; // NOTE(micha): points_r is readonly

                float t = Time.time - m_ShootTime;
                Particle particle = Particle.From(this);

                if (PreviousProjectile is ProjectileStream)
                {   // connect to previous projectile / point
                    points[0] = particle.PositionAt(t);
                    var previous = (ProjectileStream) PreviousProjectile;
                    t = Time.time - previous.m_ShootTime;
                    points[1] = Particle.From(previous).PositionAt(t);
                }
                else
                {   // connect to previous projectile / point
                    points[1] = particle.PositionAt(t);
                    float s = m_WeaponDelayBetweenShots * 2.0f;
                    t = Mathf.Max(0.0f, t - 0.2f);
                    points[0] = particle.PositionAt(t);
                }

                // Assuming the asset is set to 2 positions
                // (unity might allocate even if the size is the same)
                // Line.positionCount = points.Length;
                Line.SetPositions(points);

                // ray-cast for hit-check
                Vector3 origin = points[0];
                Vector3 direction = points[1] - points[0];
                float distance = direction.magnitude;
                if (distance > 0.0f) // only non collapsed lines
                {
                    direction /= distance;
                    RaycastHit hit;
                    if (Physics.Raycast(origin, direction, out hit, distance, HittableLayers, k_TriggerInteraction))
                    {
                        if (IsHitValid(hit))
                        {
                            OnHit(hit.point, hit.normal, hit.collider);
                        }
                    }
                }
            }
        }

        bool IsHitValid(RaycastHit hit)
        {
            // ignore hits with an ignore component
            if (hit.collider.GetComponent<IgnoreHitDetection>())
            {
                return false;
            }

            // ignore hits with triggers that don't have a Damageable component
            if (hit.collider.isTrigger && hit.collider.GetComponent<Damageable>() == null)
            {
                return false;
            }

            // ignore hits with specific ignored colliders (self colliders, by default)
            if (m_IgnoredColliders != null && m_IgnoredColliders.Contains(hit.collider))
            {
                return false;
            }

            return true;
        }

        void OnHit(Vector3 point, Vector3 normal, Collider collider)
        {
            // damage
            if (AreaOfDamage)
            {
                // area damage
                AreaOfDamage.InflictDamageInArea(Damage, point, HittableLayers, k_TriggerInteraction,
                    m_ProjectileBase.Owner);
            }
            else
            {
                // point damage
                Damageable damageable = collider.GetComponent<Damageable>();
                if (damageable)
                {
                    damageable.InflictDamage(Damage, false, m_ProjectileBase.Owner);
                }
            }

            // impact vfx
            if (ImpactVfx)
            {
                GameObject impactVfxInstance = Instantiate(ImpactVfx);
                impactVfxInstance.transform.position = point + (normal * ImpactVfxSpawnOffset);
                impactVfxInstance.transform.forward = -normal;
                if (ImpactVfxLifetime > 0)
                {
                    Destroy(impactVfxInstance.gameObject, ImpactVfxLifetime);
                }
            }

            // impact sfx
            if (ImpactSfxClip)
            {
                AudioUtility.CreateSFX(ImpactSfxClip, point, AudioUtility.AudioGroups.Impact, 1f, 3f);
            }

            // Self Destruct
            Destroy(this.gameObject);
        }

        // Simple Particle physics based on Newton's laws of motion
        struct Particle
        {
            public Vector3 InitialPosition;
            public Vector3 InitialVelocity;
            public Vector3 Acceleration;

            public Vector3 VelocityAt(float time)
            {
                Vector3 a = Acceleration;
                return a*time + InitialVelocity;
            }

            public Vector3 PositionAt(float time)
            {
                Vector3 v = VelocityAt(time);
                return v*time + InitialPosition;
            }

            public static Particle From(ProjectileStream projectile)
            {
                return new() {
                    InitialPosition = projectile.InitialPosition,
                    InitialVelocity = projectile.InitialDirection * projectile.Speed,
                    Acceleration = projectile.GravityDownAcceleration * Vector3.down,
                };
            }
        }
    }
}
